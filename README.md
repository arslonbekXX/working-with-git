# Working with git

## Git commands

    ✅ git clone (remote git -> local git)
    ✅ git checkout (change branch)
    ✅ git checkout -b feature_123 (create branch)
    ✅ git push (local git -> remote git)
    ✅ git add  (change files -> local stage )
    ✅ git commit -m "something" (commit of stages)
    ✅ git fetch (remote git -> local git)
    ✅ git merge (merging files)
    ✅ git pull (git fetch + git merge)
    ✅ git branches (feature vs origin/feature)
    ✅
